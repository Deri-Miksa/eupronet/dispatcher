export type CountryCode = "BE" | "CZ" | "GE" | "HU" | "IS" | "NO";
export type Color = "RED" | "WHITE" | "YELLOW";

type PayloadTypes = OrderItems | ColorReading | RFIDReading | QualityControl | PackagingInstruction;

export interface EPNMessage {
	//country: countryCode
	code: number,
	label?: string,
	payload?: OrderItems | ColorReading | RFIDReading | QualityControl | PackagingInstruction
}

export class NonePayload {}

export class PrintJob {
	constructor(
		public items: { color: string, amount: number }[] = []
	) {}
}

export class ColorReading {
	public constructor (
		public color: Color
	) {}
}
export class RFIDReading {
	public constructor (
		public rfid: string
	) {}
}

export class PartOfOrderResponse {
	public constructor (
		public rfid: string,
		public ordered: boolean
	) {}
}

export class QualityControl {
	public constructor (
		public rfid: string,
		public passed: boolean,
		public reason?: string
	) {}
}

export class PackagingInstruction {
	public constructor (
		public rfid: string,
		public orderid: string
	) {}
}

export class OrderID {
	public constructor (
		public orderid: string
	) {}
}

export class CodeRecord {
	readonly payloadType?: any = NonePayload
	public constructor(
		readonly label: string,
		readonly code: number,
		readonly direction: DDR,
		payloadType: any = NonePayload
	) { this.payloadType = payloadType }
}

export enum DDR {
	FROM_SERVER = 0,
	TO_SERVER = 1
}

const [ fromServer, toServer ] = [ DDR.FROM_SERVER, DDR.TO_SERVER ];

export class Workstation {
	constructor(
		readonly label: string,
		readonly country: CountryCode,
		readonly codetable: CodeRecord[],
	) {}
}

export const workstations = [
	new Workstation("printer", "CZ", [
		{ code: 102, label: "Print job", 							direction: fromServer 	, payloadType: PrintJob},
		{ code: 102, label: "Print in progress", 					direction: toServer		},
		{ code: 103, label: "Print finished", 						direction: toServer		},
		{ code: 104, label: "Assembly finished", 					direction: toServer		}
	]),
	new Workstation("colorarm", "IS", [
		{ code: 201, label: "Chip ready", 							direction: fromServer 	}, 
		{ code: 202, label: "Swing start", 							direction: toServer 	},
		{ code: 203, label: "Color reading", 						direction: toServer		, payloadType: ColorReading},
		{ code: 203, label: "Swing complete", 						direction: toServer		},
	]),
	new Workstation("rfidarm", "NO", [
		{ code: 301, label: "Chip ready", 							direction: fromServer 	}, 
		{ code: 302, label: "Swing start", 							direction: toServer 	}, 
		{ code: 303, label: "RFID reading", 						direction: toServer		, payloadType: RFIDReading},
		{ code: 304, label: "Swing complete", 						direction: toServer		},
	]),
	new Workstation("chipsorter", "GE", [
		{ code: 401, label: "Chip ready", 							direction: fromServer 	},
		{ code: 402, label: "Is chip part of an order? (request)",	direction: toServer  	, payloadType: RFIDReading},
		{ code: 412, label: "Is chip part of an order? (response)", direction: fromServer 	, payloadType: PartOfOrderResponse},
		{ code: 403, label: "Chip passed", 							direction: toServer 	, payloadType: QualityControl}
	]),
	new Workstation("packaging", "BE", [
		{ code: 501, label: "Chip ready", 							direction: fromServer 	, payloadType: PackagingInstruction},
		{ code: 502, label: "Placed in slot",						direction: toServer  	, payloadType: RFIDReading},
		{ code: 503, label: "Order assembled", 						direction: fromServer 	, payloadType: OrderID},
		{ code: 504, label: "Delivered", 							direction: toServer 	, payloadType: OrderID}
	])
];

type OrderItems = { color: Color, amount: number, amountFinished?: number }[];

interface OrderInfo {
	id: string,
	deliveryAddress?: string,
	items: OrderItems
}

interface Chip {
	rfid: string,
	color: Color
}